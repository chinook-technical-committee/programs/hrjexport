## code to prepare `Stocks` dataset goes here
Stocks <- read.csv("./inst/extdata/stocks.csv", stringsAsFactors = FALSE)
usethis::use_data(Stocks, overwrite = TRUE)
