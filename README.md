
# hrjexport

<!-- badges: start -->
<!-- badges: end -->

The goal of hrjexport is a package use to read HRJ and OUT text files and format the results into an Excel export.

**NOTE:** Because of the size of the HRJ data sets, it is recommended that you run export in 64-bit R (especially when comparing HRJ Excel Files).

## Installation

You can install the latest version of hrjexport from [GitLab](https://gitlab.com/chinook-technical-committee/programs/r-packages/hrjexport) with:

``` r
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/hrjexport") 
```

## Examples

**NOTE:** All export functions interrogate ZIP files for files with the associated extension (e.g. .HRJ or .OUT).  If they are found, then it are extracted and read for inclusion in the export.

### HRJ File Export

This is basic example reads all the HRJ files in chosen directory and all sub-directories within.  After reading all the HRJ files, the function produces a Calendar, Brood, and Combined HRJ export in Excel format:

``` r
library(hrjexport)
era_output_dir <- choose.dir()
exportHrjExcelFiles(era_output_dir)
```

### OUT File Export

This basic example reads all the OUT files in the directory and all sub-directories provided and produces an export for all files produced using the Calendar and Brood Year method (the default of the function) in Excel format.  The function has parameters to specify if Calendar year, Brood Year, or both methods should be exported to the Excel format.  The default is to export both methods as separate worksheets within an Excel workbook.

``` r
library(hrjexport)
era_output_dir <- choose.dir()
exportOutExcelFiles(era_output_dir)
```


### Compare OUT File Export

This basic example compares two OUT exports and writes the results to an Excel file:

``` r
library(hrjexport)

# Use an example of export provided in the hrjexport package
export_first_filename <- file.path(system.file("extdata", package = "hrjexport"), "CHI_OUT_Test.xlsx")

# The second example export is generated from the OUT files provide in a sample zip of OUT files 
# provided in the hrjexport package
export_second_filename <-
  exportOutExcelFiles(file.path(system.file("extdata", package = "hrjexport"), "CHI_SAMPLE.zip"),
                      output_filename = "CHI_OUT_EXPORT.xlsx",
                      limit_method = OutMthdCalendar)

# Compare the data from the two exports and provide the differences in an excel file
compareOutExports(export_first_filename,
                  export_second_filename)

```




### Compare HRJ Files

This basic example compares two HRJ file sets and writes the results to an Excel file:

``` r
library(hrjexport)

compareHrjFiles(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
                system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
                output_file_name = tempfile(fileext=".xlsx"))
```

### Compare HRJ Files Long

This basic example compares two HRJ file sets and provides the results in a data frame:

``` r
library(hrjexport)

compareHrjFilesLong(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
                    system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"))
```

## HRJ File Layout

![HRJ File layout](inst/img/hrj_file_layout.jpg)
