TerminalFTCode <- "T"
PreTerminalFTCode <- "P"

#' Pre-terminal fishery type
#'
#' @export
PreTerminalFT <- "Pre-Terminal"

#' True terminal fishery type
#'
#' @export
TrueTerminalFT <- "True Terminal"

#' Terminal ocean fishery type
#'
#' @export
TerminalOceanFT <- "Terminal Ocean"

MarineWaterType <- "M"
FreshWaterType <- "F"

NetGearType <- "N"


MrpUtilPkgName <- "mrpBaseUtils"
