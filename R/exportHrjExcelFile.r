#' Format an HRJ Data frame into a user friendly format for export
#'
#' @param hrj_df HRJ Data to format for exports
#' @param limit_stock_country Limit to "CA" for Canada, "US" for US, or NULL for all.
#' @param round_cs_or_ts Round the cs_or_ts value, used to align format with other files
#'
#' @return A HRJ data frame with additional columns describing the HRJ data
#'
#' @importFrom dplyr mutate tibble right_join inner_join if_else case_when everything
#'
formatHrjExport <- function(hrj_df,
                            limit_stock_country = NULL,
                            round_cs_or_ts = FALSE) {

  ages <- select(hrjexport::Ages, age)

  stock_info_df <-
    hrjexport::Stocks %>%
    select(stock_code,
           cyer_stock,
           stock_jurisdiction,
           stock_region,
           stock_country)

  stock_age <-
    hrjexport::Stocks %>%
    select(stock_code, stock_start_age, stock_max_age, stock_term_net_age) %>%
    cross_join(ages) %>%
    filter(age >= stock_start_age, age <= stock_max_age) %>%
    select(-stock_start_age, -stock_max_age)

  hrj_df <-
    hrj_df %>%
    inner_join(hrjexport::fisheries, ., by = "fishery_id") %>%
    right_join(stock_info_df, ., by = "stock_code") %>%
    right_join(stock_age, ., by = c("stock_code", "age")) %>%
    mutate(fishery_type_2 = case_when(gear_type == NetGearType &
                                        water_type == MarineWaterType &
                                        age >= stock_term_net_age &
                                        fishery_type_1 == PreTerminalFTCode ~ TerminalOceanFT,
                                      fishery_type_1 == PreTerminalFTCode ~ PreTerminalFT,
                                      fishery_type_1 == TerminalFTCode ~ TrueTerminalFT,
                                      TRUE ~ NA_character_),
           year = brood_year + age,
           stock_term_net_age = NULL) %>%
    select(stock_code,
           stock_country,
           stock_region,
           cyer_stock,
           stock_jurisdiction,
           fishery_id,
           fishery_name,
           region,
           management,
           fishery_type_1,
           fishery_type_2,
           age,
           brood_year,
           year,
           everything())

  if (round_cs_or_ts == TRUE) {
    hrj_df <-
      mutate(hrj_df, cs_or_ts = round(cs_or_ts))
  }

  if (!is.null(limit_stock_country)) {
    if (!limit_stock_country %in% c("CA", "US")) {
      cat(glue("Invalid country code '{limit_stock_country}'"))
      stop()
    }

    hrj_df <-
      filter(hrj_df, stock_country == limit_stock_country)
  }

  return(hrj_df)
}

#' Read the HRJ files from a directory (recursively), checks the data, and write the results to Excel
#'
#' @param hrj_file_path A single file, directory, or ZIP file containing HRJ files/
#' @param limit_stock_country Limit to "CA" for Canada, "US" for US, or NULL for all.
#' @param round_cs_or_ts Round the cs_or_ts value, used to align format with other files
#' @param inc_method_col Include a column identifying if the values where from calendar or brood method
#' @param mark_type Which type of HRJ to export, "marked" or "unmarked"
#' @param output_dir Directory to write the export file
#'
#' @return List with the calendar_file_name, brood_file_name, and combined_file_name
#'
#' @export
#'
#' @importFrom fs path_dir is_dir
#'
#' @examples
#' exportHrjExcelFiles(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
#'                    output_dir = tempdir())
#'
exportHrjExcelFiles <- function(hrj_file_path = NA,
                                limit_stock_country = NULL,
                                round_cs_or_ts = FALSE,
                                inc_method_col = TRUE,
                                mark_type = "marked",
                                output_dir = NA) {

  hrj_list <- exportHrjFiles(hrj_file_path,
                             limit_stock_country,
                             round_cs_or_ts,
                             inc_method_col,
                             mark_type)

  if(is.na(output_dir)) {
    if(is_dir(hrj_file_path[1])) {
      output_dir <- hrj_file_path[1]
    } else {
      output_dir <- path_dir(hrj_file_path[1])
    }
  }

  calendar_file_name <-
    hrj_list$calendar_hrj %>%
    writeSingleHrjExcelFile(output_dir, "Calendar")

  brood_file_name <-
    hrj_list$brood_hrj %>%
    writeSingleHrjExcelFile(output_dir, "Brood")

  combined_file_name <-
    hrj_list$combine_hrj %>%
    writeSingleHrjExcelFile(output_dir, "Combine")

  return(list(calendar_file_name = calendar_file_name,
              brood_file_name = brood_file_name,
              combined_file_name = combined_file_name))
}


#' Read the HRJ files from a directory (recursively), checks the data, and return list of data frames
#'
#' @param hrj_file_path A single file, directory, or ZIP file containing HRJ files/
#' @param limit_stock_country Limit to "CA" for Canada, "US" for US, or NULL for all.
#' @param round_cs_or_ts Round the cs_or_ts value, used to align format with other files
#' @param inc_method_col Include a column identifying if the values where from calendar or brood method
#' @param mark_type Which type of HRJ to export, "marked" or "unmarked"
#'
#' @return List with the calendar, brood_file_name, and combined_file_name
#'
#' @export
#'
#' @importFrom fs path_dir is_dir
#'
#' @examples
#' exportHrjFiles(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"))
#'
exportHrjFiles <- function(hrj_file_path = NA,
                           limit_stock_country = NULL,
                           round_cs_or_ts = FALSE,
                           inc_method_col = TRUE,
                           mark_type = "marked") {
  hrj_list <- readHrjFiles(hrj_file_path, mark_type)

  hrj_list$combine_hrj <- combineHrjData(hrj_list, inc_method_col)

  hrj_list$calendar_hrj <-
    formatHrjExport(hrj_list$calendar_hrj, limit_stock_country, round_cs_or_ts)

  hrj_list$brood_hrj <-
    formatHrjExport(hrj_list$brood_hrj, limit_stock_country, round_cs_or_ts)

  hrj_list$combine_hrj <-
    formatHrjExport(hrj_list$combine_hrj, limit_stock_country, round_cs_or_ts)

  return(hrj_list)
}

