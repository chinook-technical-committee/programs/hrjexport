
#' Compare two OUT excel exports and produce an excel file of the differences
#'
#' @param first_out_excel Path to the first excel file name with OUT data
#' @param second_out_excel Path to the second excel file name with OUT data
#' @param output_dir Path to write output comparison file
#' @param output_file_name File name to write comparison too
#'
#' @export
#'
#' @importFrom openxlsx read.xlsx getSheetNames
#'
compareOutExports <- function(first_out_excel,
                              second_out_excel,
                              output_dir = getwd(),
                              output_file_name = NULL) {
  if (!requireNamespace(MrpUtilPkgName, quietly = TRUE)) {
    stop(glue("Package \"{MrpUtilPkgName}\" needed for this function to work. Please install it."),
         call. = FALSE)
  }

  first_worksheets <- getSheetNames(first_out_excel)
  second_worksheets <- getSheetNames(second_out_excel)

  all_worksheets <-
    c(first_worksheets, second_worksheets) %>%
    unique()

  first_data_list <- list()
  second_data_list <- list()

  for(worksheet_idx in 1:length(all_worksheets)) {
    first_df <- NULL
    worksheet_name <- all_worksheets[worksheet_idx]
    if(worksheet_name %in% first_worksheets) {
      first_df <-
        read.xlsx(first_out_excel,
                  sheet = worksheet_name) %>%
        as_tibble()
    }
    first_data_list[[worksheet_name]] <- first_df

    second_df <- NULL
    if(worksheet_name %in% second_worksheets) {
      second_df <-
        read.xlsx(second_out_excel,
                  sheet = worksheet_name) %>%
        as_tibble()
    }
    second_data_list[[worksheet_name]] <- second_df
  }

  if (is.null(output_file_name)) {
    output_file_name <- "OUT_diff.xlsx"
  }

  output_file_name <- path(output_dir, output_file_name)

  mrpBaseUtils::compareDataFramesOutput(first_data_list,
                                        second_data_list,
                                        key_cols = c("stock_code",
                                                     "brood_year",
                                                     "age"),
                                        output_file_name = output_file_name,
                                        output_format = mrpBaseUtils::MrpOutputExcel,
                                        numeric_diff = "abs")
}
